from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import progressbar
import time
import pickle
from settings import *
import sys
import datetime
import os


########################################################################################################################
def get_fileNames_from_directory(directory='/Users/csu/Desktop/Neuron/nrn_original', file_type=None):
    file_lst = []
    if directory.endswith("/"):
        directory = directory[:-1]

    for (dirpath, dirnames, filenames) in os.walk(directory):
        file_lst.extend(filenames)
        break

    if all([file_type is not None, file_type != 'all']):
        if type(file_type) is list:
            lst = []
            for t in file_type:
                lst_temp = [x for x in file_lst if x.endswith('.' + t)]
                lst = list(set(lst)|set(lst_temp))
            file_lst = lst

        elif type(file_type) is str:
            file_lst = [x for x in file_lst if x.endswith('.' + file_type)]
    # ex. file_type = 'swc' or 'csv' or ['txt', 'csv', 'swc']


    for t in ['.localized', '.DS_Store']:
        if t in file_lst:
            file_lst.remove(t)
        else:
            pass

    file_lst.sort()

    return file_lst


########################################################################################################################
def delete_files_under_directory(directory='/Users/csu/Desktop/Neuron/nrn_plot', file_type='gv'):
    file_lst = []
    if directory.endswith("/"):
        directory = directory[:-1]

    for (dirpath, dirnames, filenames) in os.walk(directory):
        file_lst.extend(filenames)
        break

    if all([file_type is not None, file_type != 'all']):
        if type(file_type) is list:
            lst = []
            for t in file_type:
                lst_temp = [x for x in file_lst if x.endswith('.' + t)]
                lst = list(set(lst)|set(lst_temp))
            file_lst = lst

        elif type(file_type) is str:
            file_lst = [x for x in file_lst if x.endswith('.' + file_type)]
    # ex. file_type = 'swc' or 'csv' or ['txt', 'csv', 'swc']


    for file in file_lst:
        os.remove(directory + '/' + file)

    return


########################################################################################################################
