from util import *


# ----------------------------------------------------------------------
#  MAIN PART OF THE CODE
# ----------------------------------------------------------------------
# Getting back the objects:

data_lst = get_fileNames_from_directory(directory=DATA_DIR, file_type=None)


nt = 0
T0 = []
E0 = []
M0 = []
C0 = []
X0 = []


for d in data_lst:
    with open(DATA_DIR + d, 'rb') as f:  # Python 3: open(..., 'rb')
        T, E, M, C, X, Configs = pickle.load(f)

    nt += len(T)
    T = T.tolist()
    T0 += T
    E = E.tolist()
    E0 += E
    M = M.tolist()
    M0 += M
    C = C.tolist()
    C0 += C
    X = X.tolist()
    X0 += X



T0 = np.array(T0)
E0 = np.array(E0)
M0 = np.array(M0)
C0 = np.array(C0)
X0 = np.array(X0)


# Saving the objects:
file_name = 'objs_%d.pkl'%nt
with open(DATA_DIR + file_name, 'wb') as f:  # Python 3: open(..., 'wb')
    pickle.dump([T0, E0, M0, C0, X0], f)